all:
	rm -f MyStrategy
	rm -f MyStrategy.zip
	g++ -Wall -static -fno-optimize-sibling-calls -fno-strict-aliasing -DONLINE_JUDGE -D_LINUX -lm -s -x c++ -O2 -o MyStrategy MyStrategy.cpp RemoteProcessClient.cpp Runner.cpp Strategy.cpp model/Bonus.cpp model/Move.cpp model/Obstacle.cpp model/PlayerContext.cpp model/Player.cpp model/Shell.cpp model/Tank.cpp model/Unit.cpp model/World.cpp csimplesocket/ActiveSocket.cpp csimplesocket/HTTPActiveSocket.cpp csimplesocket/PassiveSocket.cpp csimplesocket/SimpleSocket.cpp
	zip MyStrategy.zip MyStrategy.h MyStrategy.cpp help.h