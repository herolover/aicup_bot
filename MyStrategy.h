#pragma once

#ifndef _MY_STRATEGY_H_
#define _MY_STRATEGY_H_

#include <vector>
#include <map>
#include <utility>

#include "Strategy.h"
#include "help.h"

using namespace std;
using namespace model;


struct Corner
{
  Corner(double x_, double y_)
    : x(x_)
    , y(y_)
  {
  }

  double x;
  double y;
};


class NearestUnit
{
public:
  NearestUnit(const Unit &target)
    : x_(target.x())
    , y_(target.y())
  {
  }

  NearestUnit(double x, double y)
    : x_(x)
    , y_(y)
  {
  }

  bool operator () (const Unit &unit0, const Unit &unit1) const
  {
    return unit0.GetDistanceTo(this->x_, this->y_) < unit1.GetDistanceTo(this->x_, this->y_);
  }

  bool operator () (const Corner &corner0, const Corner &corner1) const
  {
    return get_distance(corner0.x, corner0.y, this->x_, this->y_)
           < get_distance(corner1.x, corner1.y, this->x_, this->y_);
  }

private:
  double x_;
  double y_;
};


class GreaterAngle
{
public:
  GreaterAngle(const Unit &target, double angle)
    : target_(target)
    , angle_(angle)
  {
  }

  bool operator () (const Unit &unit) const
  {
    return fabs(unit.GetAngleTo(target_)) * 180.0 / M_PI > this->angle_;
  }

private:
  const Unit &target_;
  double angle_;
};


class GreaterDistance
{
public:
  GreaterDistance(const Unit &target, double distance)
    : x_(target.x())
    , y_(target.y())
    , distance_(distance)
  {
  }

  GreaterDistance(double x, double y, double distance)
    : x_(x)
    , y_(y)
    , distance_(distance)
  {
  }

  bool operator () (const Unit &unit) const
  {
    return unit.GetDistanceTo(this->x_, this->y_) > this->distance_;
  }

private:
  double x_;
  double y_;
  double distance_;
};


class MostAttacked
{
public:
  MostAttacked(const vector<Tank> &tanks, double max_angle)
    : tanks_(tanks)
    , max_angle_(max_angle)
  {
  }

  unsigned count_attackers(const Tank &tank) const
  {
    unsigned count = 0;
    for (unsigned i = 0; i < this->tanks_.size(); ++i)
      if (this->tanks_[i].id() != tank.id()
          && fabs(this->tanks_[i].GetTurretAngleTo(tank)) * 180.0 / M_PI < this->max_angle_)
        count += 1;

    return count;
  }

  bool operator () (const Tank &tank0, const Tank &tank1) const
  {
    return this->count_attackers(tank0) > this->count_attackers(tank1);
  }

private:
  vector<Tank> tanks_;
  double max_angle_;
};


class MostSafely
{
public:
  MostSafely(const vector<Tank> &tanks, double max_angle)
    : tanks_(tanks)
    , max_angle_(max_angle)
  {
  }

  unsigned count_attackers(const Bonus &bonus) const
  {
    unsigned count = 0;
    for (unsigned i = 0; i < this->tanks_.size(); ++i)
      if (fabs(this->tanks_[i].GetTurretAngleTo(bonus)) * 180.0 / M_PI < this->max_angle_)
        count += 1;

    return count;
  }

  bool operator () (const Bonus &bonus0, const Bonus &bonus1) const
  {
    return this->count_attackers(bonus0) < this->count_attackers(bonus1);
  }

private:
  vector<Tank> tanks_;
  double max_angle_;
};


inline
bool Dead(const Tank &tank)
{
  return tank.crew_health() == 0 || tank.hull_durability() == 0;
}


class MatchID
{
public:
  MatchID(const long id)
    : id_(id)
  {
  }

  bool operator () (const Unit &unit) const
  {
    return this->id_ == unit.id();
  }

private:
  const long id_;
};


class MatchType
{
public:
  MatchType(const BonusType bonus_type)
    : bonus_type_(bonus_type)
  {
  }

  bool operator () (const Bonus &bonus) const
  {
    return this->bonus_type_ == bonus.type();
  }

private:
  const BonusType bonus_type_;
};


class CountTanks
{
public:
  CountTanks(const vector<Tank> &tanks, double radius)
    : tanks_(tanks)
    , radius_(radius)
  {
  }

  bool operator () (const Corner &corner0, const Corner &corner1) const
  {
    unsigned count0 = 0;
    unsigned count1 = 0;

    for (unsigned i = 0; i < this->tanks_.size(); ++i)
    {
      if (get_distance(corner0.x, corner0.y, this->tanks_[i].x(), this->tanks_[i].y()) < this->radius_)
        count0 += 1;

      if (get_distance(corner1.x, corner1.y, this->tanks_[i].x(), this->tanks_[i].y()) < this->radius_)
        count1 += 1;
    }

    return count0 < count1;
  }

private:
  vector<Tank> tanks_;
  double radius_;
};


struct Point
{
  double x;
  double y;
};


struct UnitInfo
{
  double pos_0_x;
  double pos_0_y;
  double pos_1_x;
  double pos_1_y;
  double pos_2_x;
  double pos_2_y;


  UnitInfo()
    : pos_0_x(0.0)
    , pos_0_y(0.0)
    , pos_1_x(0.0)
    , pos_1_y(0.0)
    , pos_2_x(0.0)
    , pos_2_y(0.0)
  {
  }

  inline
  double old_speed_x()
  {
    return this->pos_1_x - this->pos_0_x;
  }

  inline
  double old_speed_y()
  {
    return this->pos_1_y - this->pos_0_y;
  }

  inline
  double speed_x()
  {
    return this->pos_2_x - this->pos_1_x;
  }

  inline
  double speed_y()
  {
    return this->pos_2_y - this->pos_1_y;
  }

  inline
  double accel_x()
  {
    return this->speed_x() - this->old_speed_x();
  }

  inline
  double accel_y()
  {
    return this->speed_y() - this->old_speed_y();
  }

  inline
  void update(double x, double y)
  {
    this->pos_0_x = this->pos_1_x;
    this->pos_0_y = this->pos_1_y;
    this->pos_1_x = this->pos_2_x;
    this->pos_1_y = this->pos_2_y;
    this->pos_2_x = x;
    this->pos_2_y = y;
  }
};



class MyStrategy: public Strategy
{
public:
  MyStrategy()
    : Strategy()
    , front_dir_to_avoid(true)
  {
  }

  void update_unit_info(const World &world);

  bool can_attack(const Tank &self, const World &world, double x, double y) const;

  Tank get_tank_by_id(const World &world, long id) const;
  Bonus get_bonus_by_id(const World &world, long id) const;
  bool no_bonus_with_id(const World &world, long id) const;

  double get_distance_to_bound(const Tank &self) const;
  void get_nearest_safely_corner_coords(const Tank &self, const World &world,
                                        double &x, double &y) const;
  void get_turret_coords(const Tank &tank, double &x, double &y) const;

  void look_to(const Tank &self, model::Move &move, double x, double y);
  void move_to(const Tank &self, model::Move &move, double x, double y);
  void rotate_on(const Tank &self, model::Move &move, double angle);

  Tank select_tank_target(const Tank &self, const World &world) const;
  void specify_target(double x, double y, double shell_speed, const Unit &target,
                      double &target_x, double &target_y);
  void attack(const Tank &self, const World &world, model::Move &move,
              const Unit &target);

  void Move(Tank self, World world, model::Move& move);
  TankType SelectTank(int tank_index, int team_size);


  bool front_dir_to_avoid;
  map<long, Point> shell_target;
  map<long, UnitInfo> unit_info;
};

#endif
