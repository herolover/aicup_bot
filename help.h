#ifndef HELP_H
#define HELP_H


#define _USE_MATH_DEFINES
#include <cmath>


inline
double sqr(double x)
{
  return x * x;
}


inline
double get_length(double x, double y)
{
  return sqrt(sqr(x) + sqr(y));
}


inline
double get_distance(double x0, double y0, double x1, double y1)
{
  return get_length(x1 - x0, y1 - y0);
}


inline
double sign(double v)
{
  if (v < 0.0)
    return -1.0;
  else if (v > 0.0)
    return 1.0;
  else
    return 0.0;
}


#endif // HELP_H
