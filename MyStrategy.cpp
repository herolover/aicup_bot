#include <algorithm>
#include <iostream>
#include <fstream>

#include "MyStrategy.h"


void rotate(double &x, double &y, double angle)
{
  double x_n = x * cos(angle) - y * sin(angle);
  double y_n = x * sin(angle) + y * cos(angle);

  x = x_n;
  y = y_n;
}


void MyStrategy::update_unit_info(const World &world)
{
  vector<Tank> tanks = world.tanks();
  for (unsigned i = 0; i < tanks.size(); ++i)
    this->unit_info[tanks[i].id()].update(tanks[i].x(), tanks[i].y());
}


vector<Point> get_unit_corners(const Unit &unit, const Point &origin)
{
  vector<Point> corners;

  Point corner;
  corner.x = unit.width() * 0.5;
  corner.y = unit.height() * 0.5;
  corners.push_back(corner);
  corner.x = unit.width() * 0.5;
  corner.y = -unit.height() * 0.5;
  corners.push_back(corner);
  corner.x = -unit.width() * 0.5;
  corner.y = -unit.height() * 0.5;
  corners.push_back(corner);
  corner.x = -unit.width() * 0.5;
  corner.y = unit.height() * 0.5;
  corners.push_back(corner);

  for (unsigned i = 0; i < 4; ++i)
  {
    rotate(corners[i].x, corners[i].y, unit.angle());
    corners[i].x += origin.x;
    corners[i].y += origin.y;
  }

  return corners;
}


inline
double cross_product(const Point &a, const Point &b)
{
  return a.x * b.y - a.y * b.x;
}


inline
Point get_normalized(const Point &a)
{
  double length = get_length(a.x, a.y);

  Point n(a);
  n.x /= length;
  n.y /= length;

  return n;
}


bool MyStrategy::can_attack(const Tank &self, const World &world,
                            double x, double y) const
{
  Point turret_coords;
  this->get_turret_coords(self, turret_coords.x, turret_coords.y);

  Shell shell(12345, "", 22.5, 7.5, turret_coords.x, turret_coords.y, 0.0, 0.0,
              self.angle() + self.turret_relative_angle(), 0.0, REGULAR);
  vector<Point> shell_corners = get_unit_corners(shell, turret_coords);

  Point dir;
  dir.x = 1.0;
  dir.y = 0.0;
  rotate(dir.x, dir.y, shell.angle());

  vector<Bonus> bonuses = world.bonuses();
  for (unsigned i = 0; i < bonuses.size(); ++i)
    if (fabs(shell.GetAngleTo(bonuses[i])) < M_PI * 0.5)
    {
      vector<Point> corners_on_left_side = get_unit_corners(bonuses[i], shell_corners[2]);
      vector<Point> corners_on_right_side = get_unit_corners(bonuses[i], shell_corners[3]);

      double left = 1.0;
      double right = 1.0;
      for (unsigned j = 0; j < 4; ++j)
      {
        left *= cross_product(dir, get_normalized(corners_on_left_side[j]));
        right *= cross_product(dir, get_normalized(corners_on_right_side[j]));
      }

      cerr << left << " " << right << endl;

      if ((left < 0.0 || right < 0.0) && shell.GetDistanceTo(bonuses[i]) < shell.GetDistanceTo(x, y))
        return false;
    }

  return true;
}


Tank MyStrategy::get_tank_by_id(const World &world, long id) const
{
  vector<Tank> tanks = world.tanks();
  for (unsigned i = 0; i < tanks.size(); ++i)
    if (tanks[i].id() == id)
      return tanks[i];
}


Bonus MyStrategy::get_bonus_by_id(const World &world, long id) const
{
  vector<Bonus> bonuses = world.bonuses();
  for (unsigned i = 0; i < bonuses.size(); ++i)
    if (bonuses[i].id() == id)
      return bonuses[i];
}


bool MyStrategy::no_bonus_with_id(const World &world, long id) const
{
  vector<Bonus> bonuses = world.bonuses();
  for (unsigned i = 0; i < bonuses.size(); ++i)
    if (bonuses[i].id() == id)
      return true;

  return false;
}


double MyStrategy::get_distance_to_bound(const Tank &self) const
{
  double dir_x = 1.0;
  double dir_y = 0.0;
  rotate(dir_x, dir_y, self.angle());

  double x = self.x();
  double y = self.y();

  double distance = 0.0;
  while (x > 0.0 && x < 1280.0 && y > 0.0 && y < 800.0)
  {
    x += dir_x;
    y += dir_y;

    distance += 1.0;
  }

  return distance;
}


void MyStrategy::get_nearest_safely_corner_coords(const Tank &self,
                                                  const World &world,
                                                  double &x, double &y) const
{
  vector<Corner> corners;
  double bias = 150.0;
  corners.push_back(Corner(bias,  bias));
  corners.push_back(Corner(1280.0 - bias,  bias));
  corners.push_back(Corner(1280.0 - bias, 800.0 - bias));
  corners.push_back(Corner(bias, 800.0 - bias));

  sort(corners.begin(), corners.end(), NearestUnit(self));
  vector<Tank> tanks = world.tanks();
  tanks.erase(remove_if(tanks.begin(), tanks.end(), MatchID(self.id())), tanks.end());
  sort(corners.begin(), corners.end(), CountTanks(tanks, 600.0));

  x = corners.front().x;
  y = corners.front().y;
}


void MyStrategy::get_turret_coords(const Tank &tank, double &x, double &y) const
{
  x = tank.x();
  y = tank.y();

  double turret_x = tank.virtual_gun_length();
  double turret_y = 0.0;
  rotate(turret_x, turret_y, tank.turret_relative_angle() + tank.angle());

  x += turret_x;
  y += turret_y;
}


void MyStrategy::look_to(const Tank &self, model::Move &move, double x, double y)
{
  move.set_turret_turn(self.GetTurretAngleTo(x, y));
}


void MyStrategy::rotate_on(const Tank &self, model::Move &move, double angle)
{
  double angle_90 = fabs(angle);
  if (angle_90 > 90.0)
    angle_90 = 180.0 - angle_90;

  double force = angle_90 * 1.2 / 90.0;

  double back_coef;
  if (self.type() == MEDIUM)
    back_coef = 0.75;
  else if (self.type() == HEAVY)
    back_coef = 0.5;
  else
    back_coef = 0.35;

  if (fabs(angle) < 90.0)
  {
    move.set_left_track_power(back_coef * force * sign(angle));
    move.set_right_track_power(-force * sign(angle));
  }
  else
  {
    move.set_left_track_power(-force * sign(angle));
    move.set_right_track_power(back_coef * force * sign(angle));
  }
}


void MyStrategy::move_to(const Tank &self, model::Move &move, double x, double y)
{
  double angle = self.GetAngleTo(x, y) * 180.0 / M_PI;

  this->rotate_on(self, move, angle);

  double angle_90 = fabs(angle);
  if (angle_90 > 90.0)
    angle_90 = 180 - angle_90;

  double force = 10.0 / angle_90;

  double s = -sign(fabs(angle) - 90.0);
  move.set_left_track_power(move.left_track_power() + s * force);
  move.set_right_track_power(move.right_track_power() + s * force);
}


Tank MyStrategy::select_tank_target(const Tank &self, const World &world) const
{
  static long tank_target_id = -1;

  vector<Tank> tanks = world.tanks();
  tanks.erase(remove_if(tanks.begin(), tanks.end(), Dead), tanks.end());
  tanks.erase(remove_if(tanks.begin(), tanks.end(), MatchID(self.id())), tanks.end());
  sort(tanks.begin(), tanks.end(), NearestUnit(self));

  if (find_if(tanks.begin(), tanks.end(), MatchID(tank_target_id)) == tanks.end())
    tank_target_id = -1;

  if (tank_target_id == -1)
    tank_target_id = tanks[0].id();

  return get_tank_by_id(world, tank_target_id);
}


void MyStrategy::specify_target(double x, double y, double shell_speed,
                                const Unit &target,
                                double &target_x, double &target_y)
{
  target_x = target.x();
  target_y = target.y();
  for (unsigned i = 0; i < 5; ++i)
  {
    double fly_time = get_distance(x, y, target_x, target_y) / shell_speed;

    target_x = target.x() + this->unit_info[target.id()].speed_x() * fly_time + this->unit_info[target.id()].accel_x() * sqr(fly_time) * 0.5;
    target_y = target.y() + this->unit_info[target.id()].speed_y() * fly_time + this->unit_info[target.id()].accel_y() * sqr(fly_time) * 0.5;
  }
}


void MyStrategy::attack(const Tank &self, const World &world, model::Move &move,
                        const Unit &target)
{
  double turret_x;
  double turret_y;
  this->get_turret_coords(self, turret_x, turret_y);

  double shell_speed = 16.7;
  if (self.premium_shell_count() > 0 && self.GetDistanceTo(target) < 600.0)
    shell_speed = 13.3;

  double target_x;
  double target_y;
  specify_target(turret_x, turret_y, shell_speed, target, target_x, target_y);
  look_to(self, move, target_x, target_y);

  vector<Tank> tanks = world.tanks();
  tanks.erase(remove_if(tanks.begin(), tanks.end(), Dead), tanks.end());
  tanks.erase(remove_if(tanks.begin(), tanks.end(), MatchID(self.id())), tanks.end());
  for (unsigned i = 0; i < tanks.size(); ++i)
  {
    this->get_turret_coords(self, turret_x, turret_y);

    FireType fire_type = REGULAR_FIRE;
    shell_speed = 16.7;
    if (self.premium_shell_count() > 0 && self.GetDistanceTo(target) < 600.0)
    {
      fire_type = PREMIUM_FIRE;
      shell_speed = 13.3;
    }

    specify_target(turret_x, turret_y, shell_speed, target, target_x, target_y);

    double min_angle = 5.0 * 100.0 / self.GetDistanceTo(target_x, target_y);
    if (min_angle < 1.0)
      min_angle = 1.0;

    if (fabs(self.GetTurretAngleTo(target_x, target_y) * 180.0 / M_PI) < min_angle)
    {
      if (this->can_attack(self, world, target_x, target_y))
      {
        move.set_fire_type(fire_type);
        break;
      }
      else
        cerr << "Can't attack" << endl;
    }
  }
}


void MyStrategy::Move(Tank self, World world, model::Move &move)
{
  this->update_unit_info(world);

  if (world.tick() == 2)
    move.set_fire_type(REGULAR_FIRE);

  if (world.tick() < 80)
  {
    move.set_left_track_power(-1.0);
    move.set_right_track_power(-1.0);
  }
  else
  {
    double max_distance;
    vector<Tank> tanks = world.tanks();
    tanks.erase(remove_if(tanks.begin(), tanks.end(), Dead), tanks.end());
    tanks.erase(remove_if(tanks.begin(), tanks.end(), MatchID(self.id())),
                tanks.end());
    if (tanks.size() == 5)
      max_distance = 250.0;
    else if (tanks.size() == 4)
      max_distance = 300.0;
    else if (tanks.size() == 3)
      max_distance = 400.0;
    else if (tanks.size() == 2)
      max_distance = 500.0;
    else
      max_distance = 600.0;

    vector<Bonus> bonuses = world.bonuses();
    bonuses.erase(remove_if(bonuses.begin(), bonuses.end(),
                            GreaterDistance(self, max_distance)),
                  bonuses.end());
    sort(bonuses.begin(), bonuses.end(), NearestUnit(self));

    vector<Bonus>::iterator bonus_it = bonuses.end();

    bool need_health = self.crew_health() < self.crew_max_health();
    bool need_armor = self.hull_durability() < self.hull_max_durability();
    double health = (double)self.crew_health() / (double)self.crew_max_health();
    double armor = (double)self.hull_durability() / (double)self.hull_max_durability();

    if (need_health && (health < armor || armor / health > 0.5))
      bonus_it = find_if(bonuses.begin(), bonuses.end(), MatchType(MEDIKIT));

    if (bonus_it == bonuses.end() && need_armor)
    {
      bonus_it = find_if(bonuses.begin(), bonuses.end(), MatchType(REPAIR_KIT));

      if (bonus_it == bonuses.end() && need_health)
        bonus_it = find_if(bonuses.begin(), bonuses.end(), MatchType(MEDIKIT));
    }

    if (bonus_it == bonuses.end() && self.premium_shell_count() == 0)
    {
      bonus_it = find_if(bonuses.begin(), bonuses.end(), MatchType(AMMO_CRATE));
      if (bonus_it != bonuses.end() && self.GetDistanceTo(*bonus_it) > 300.0)
        bonus_it = bonuses.end();
    }

    if (bonus_it != bonuses.end())
      this->move_to(self, move, bonus_it->x(), bonus_it->y());
    else
    {
      double corner_x;
      double corner_y;
      this->get_nearest_safely_corner_coords(self, world, corner_x, corner_y);

      if (self.GetDistanceTo(corner_x, corner_y) > 50.0)
        move_to(self, move, corner_x, corner_y);
      else
      {
        double avg_dir_x = 0.0;
        double avg_dir_y = 0.0;
        unsigned avg_count = 0;

        vector<Tank> tanks = world.tanks();
        tanks.erase(remove_if(tanks.begin(), tanks.end(), Dead), tanks.end());
        tanks.erase(remove_if(tanks.begin(), tanks.end(), MatchID(self.id())), tanks.end());
        if (tanks.size() == 1)
        {
          avg_dir_x += self.x() - tanks[0].x();
          avg_dir_y += self.y() - tanks[0].y();
          avg_count += 1;
        }
        else
          for (unsigned i = 0; i < tanks.size(); ++i)
            if (fabs(tanks[i].GetTurretAngleTo(self)) * 180.0 / M_PI < 10.0)
            {
              double x = 1.0;
              double y = 0.0;
              rotate(x, y, tanks[i].turret_relative_angle() + tanks[i].angle());

              avg_dir_x += x;
              avg_dir_y += y;
              avg_count += 1;
            }

        if (avg_count > 0)
        {
          avg_dir_x /= avg_count;
          avg_dir_y /= avg_count;

          rotate(avg_dir_x, avg_dir_y, M_PI * 0.5);

          double x;
          double y;

          x = self.x() + avg_dir_x;
          y = self.y() + avg_dir_y;
          double angle0 = self.GetAngleTo(x, y);

          x = self.x() - avg_dir_x;
          y = self.y() - avg_dir_y;
          double angle1 = self.GetAngleTo(x, y);

          double angle;
          if (fabs(angle0) < fabs(angle1))
            angle = angle0;
          else
            angle = angle1;

          this->rotate_on(self, move, angle * 180 / M_PI);
        }
      }
    }

    vector<Shell> shells = world.shells();
    shells.erase(remove_if(shells.begin(), shells.end(), GreaterAngle(self, 15.0)), shells.end());
  //  sort(shells.begin(), shells.end(), NearestUnit(self));

    if (shells.size() > 0)
    {
      //! attention
      double distance = this->get_distance_to_bound(self);
      if (distance < 150.0)
        this->front_dir_to_avoid = !this->front_dir_to_avoid;

      double dir = 1.0;
      if (!this->front_dir_to_avoid)
        dir = -1.0;

      move.set_left_track_power(dir);
      move.set_right_track_power(dir);
    }
  }

  if (world.tick() > 2)
    this->attack(self, world, move, select_tank_target(self, world));
}


TankType MyStrategy::SelectTank(int tank_index, int team_size)
{
  return MEDIUM;
}
